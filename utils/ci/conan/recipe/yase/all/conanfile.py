################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building yase with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm
from conan.tools.scm import Git

required_conan_version = ">=1.53"

class YaseConan(ConanFile):
    name = "yase"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False], "yase_TEST": [True, False]}
    default_options = {"shared": True, "fPIC": False, "yase_TEST": False}
    exports_sources = "*"
    short_paths = True
    no_copy_source = False

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["Yase_BUILD_TESTS"] = self.options.yase_TEST
        tc.generate()

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version

        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)

    def package_info(self):
        self.cpp_info.components["agnostic_behavior_tree"].set_property("cmake_target_name", "Yase::libagnostic_behavior_tree")
