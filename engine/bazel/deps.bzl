load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

ECLIPSE_GITLAB = "https://gitlab.eclipse.org/eclipse"

MANTLE_API_TAG = "v3.0.1"
UNITS_TAG = "2.3.3"
YASE_TAG = "v0.0.1"

def osc_engine_deps():
    """Load dependencies"""
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = MANTLE_API_TAG),
        sha256 = "5e11a1b035243aa19f188c1f98466c4f38c84cbe07ec274a06f88382a5f9797e",
        strip_prefix = "mantle-api-{tag}".format(tag = MANTLE_API_TAG),
    )

    maybe(
        http_archive,
        name = "units_nhh",
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{tag}.tar.gz".format(tag = UNITS_TAG),
        sha256 = "b1f3c1dd11afa2710a179563845ce79f13ebf0c8c090d6aa68465b18bd8bd5fc",
        strip_prefix = "./units-{tag}".format(tag = UNITS_TAG),
        build_file = "//bazel:units.BUILD",
    )

    maybe(
        http_archive,
        name = "yase",
        url = ECLIPSE_GITLAB + "/openpass/yase/-/archive/{tag}/yase-{tag}.tar.gz".format(tag = YASE_TAG),
        sha256 = "243d710172dff4c4e7ab95e5bd68a6d8335cf8a3fdd1efa87d118250e3b85ee3",
        strip_prefix = "yase-{tag}".format(tag = YASE_TAG),
    )

    http_archive(
        name = "open_scenario_parser",
        build_file = "//bazel:openscenario_api.BUILD",
        url = "https://github.com/RA-Consulting-GmbH/openscenario.api.test/releases/download/v1.3.0/OpenSCENARIO_API_LinuxSharedRelease_2022.10.19.tgz",
        sha256 = "a15ee746f75d49291bc7068e4193d03b5c3e8bb81c7676885d207428f7536aa6",
        strip_prefix = "OpenSCENARIO_API_LinuxSharedRelease",
    )
