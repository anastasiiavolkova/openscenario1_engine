
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>
#include <openScenarioLib/generated/v1_2/catalog/CatalogHelperV1_2.h>

#include <memory>
#include <optional>
#include <string>

namespace OpenScenarioEngine::v1_2
{
using TrajectoryRef = std::optional<mantle_api::Trajectory>;

TrajectoryRef ConvertScenarioTrajectoryRef(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                           const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrajectoryRef>& trajectoryRef,
                                           const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICatalogReference>& catalogReference);

}  // namespace OpenScenarioEngine::v1_2