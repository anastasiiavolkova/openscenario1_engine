
/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioEnvironment.h"

#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>
#include <openScenarioLib/generated/v1_2/catalog/CatalogHelperV1_2.h>

#include <cmath>

namespace OpenScenarioEngine::v1_2
{
namespace detail
{

template <typename T>
T Discretize(const double current_value, const std::vector<std::pair<double, T>> thresholds)
{
  for (const auto& [threshold, discrete_value] : thresholds)
  {
    if (current_value < threshold)
    {
      return discrete_value;
    }
  }
  return T::kOther;
}

mantle_api::Fog ConvertFog(const double range)
{
  return Discretize(range, std::vector<std::pair<double, mantle_api::Fog>>{
                                       {0, mantle_api::Fog::kOther},
                                       {50.0, mantle_api::Fog::kDense},
                                       {200, mantle_api::Fog::kThick},
                                       {1000, mantle_api::Fog::kLight},
                                       {2000, mantle_api::Fog::kMist},
                                       {4000, mantle_api::Fog::kPoorVisibility},
                                       {10000, mantle_api::Fog::kModerateVisibility},
                                       {40000, mantle_api::Fog::kGoodVisibility},
                                       {std::numeric_limits<double>::max(), mantle_api::Fog::kExcellentVisibility}});
}

mantle_api::Precipitation ConvertPrecipitation(const double intensity)
{
  return Discretize(intensity, std::vector<std::pair<double, mantle_api::Precipitation>>{
                                           {0, mantle_api::Precipitation::kOther},
                                           {0.1, mantle_api::Precipitation::kNone},
                                           {0.5, mantle_api::Precipitation::kVeryLight},
                                           {1.9, mantle_api::Precipitation::kLight},
                                           {8.1, mantle_api::Precipitation::kModerate},
                                           {34, mantle_api::Precipitation::kHeavy},
                                           {149, mantle_api::Precipitation::kVeryHeavy},
                                           {std::numeric_limits<double>::max(), mantle_api::Precipitation::kExtreme}});
}

mantle_api::Illumination ConvertIllumination(const double illuminance)
{
  return Discretize(illuminance, std::vector<std::pair<double, mantle_api::Illumination>>{
                                             {0, mantle_api::Illumination::kOther},
                                             {0.001, mantle_api::Illumination::kLevel1},
                                             {0.01, mantle_api::Illumination::kLevel1},
                                             {1, mantle_api::Illumination::kLevel2},
                                             {3, mantle_api::Illumination::kLevel3},
                                             {10, mantle_api::Illumination::kLevel4},
                                             {20, mantle_api::Illumination::kLevel5},
                                             {400, mantle_api::Illumination::kLevel6},
                                             {1000, mantle_api::Illumination::kLevel7},
                                             {10000, mantle_api::Illumination::kLevel8},
                                             {120000, mantle_api::Illumination::kLevel9},
                                             {std::numeric_limits<double>::max(), mantle_api::Illumination::kLevel9}});
}

[[ nodiscard ]] mantle_api::Weather ConvertWeather(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IWeather> weather)
{
  mantle_api::Weather mantle_weather = GetDefaultWeather();
  return {weather->IsSetFog()
              ? ConvertFog(weather->GetFog()->GetVisualRange())
              : mantle_weather.fog,
          weather->IsSetPrecipitation() && weather->GetPrecipitation()->IsSetPrecipitationIntensity()
              ? ConvertPrecipitation(weather->GetPrecipitation()->GetPrecipitationIntensity())
              : mantle_weather.precipitation,
          weather->IsSetSun() && weather->GetSun()->IsSetIlluminance()
              ? ConvertIllumination(weather->GetSun()->GetIlluminance())
              : mantle_weather.illumination,
          mantle_weather.humidity,
          weather->IsSetTemperature()
              ? units::temperature::kelvin_t{weather->GetTemperature()}
              : mantle_weather.temperature,
          weather->IsSetAtmosphericPressure()
              ? units::pressure::pascal_t{weather->GetAtmosphericPressure()}
              : mantle_weather.atmospheric_pressure};
}

long long GetSeconds(const NET_ASAM_OPENSCENARIO::DateTime& dateTime)
{
	struct tm timestruct;
	timestruct.tm_year = dateTime.year;;
	timestruct.tm_mday = dateTime.mday;
	timestruct.tm_mon = dateTime.mon;
	timestruct.tm_hour = dateTime.hour;
	timestruct.tm_min = dateTime.min;
	timestruct.tm_sec = (int) std::ceil(dateTime.sec);
	timestruct.tm_isdst = 0;

	time_t localTime = mktime(&timestruct);

	localTime -= (dateTime.gmtHours * 3600);
	localTime -= (dateTime.gmtMin * 60);
	return localTime;
}

const NET_ASAM_OPENSCENARIO::v1_2::IEnvironment& ResolveChoice(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEnvironment>& environment,
                                                               const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICatalogReference>& catalogReference)
{
  if (environment)
  {
    return *environment;
  }

  if (catalogReference)
  {
    auto ref = catalogReference->GetRef();
    return *NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::AsEnvironment(ref);
  }

  throw std::runtime_error("ConvertScenarioEnvironment: No environment defined or referenced. Please adjust the scenario.");
}

Environment ConvertEnvironment(const NET_ASAM_OPENSCENARIO::v1_2::IEnvironment& scenario_environment)
{
  return
  {
    scenario_environment.IsSetTimeOfDay()
        ? std::optional(units::make_unit<units::time::second_t>(
                        GetSeconds(scenario_environment.GetTimeOfDay()->GetDateTime())))
        : std::nullopt,
    scenario_environment.IsSetWeather()
        ? std::optional(ConvertWeather(scenario_environment.GetWeather()))
        : std::nullopt
  };
}
}  // namespace detail

Environment ConvertScenarioEnvironment(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEnvironment>& environment,
                                       const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICatalogReference>& catalogReference)
{
  return detail::ConvertEnvironment(
      detail::ResolveChoice(environment, catalogReference));
}

}  // namespace OpenScenarioEngine::v1_2
