################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Find Package Adapter for the Antlr4 Runtime
#
# Creates the follwoing imported targets (if available):
# - antlr4_runtime::shared

set(ANTLR4_RUNTIME_SHARED_NAMES
  antlr4-runtime.lib
  libantlr4-runtime.dll.a
  libantlr4-runtime.so
)

find_library(ANTLR4_RUNTIME_SHARED_LIBRARY NAMES ${ANTLR4_RUNTIME_SHARED_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(ANTLR4_RUNTIME_SHARED_LIBRARY)
  get_filename_component(ANTLR4_RUNTIME_SHARED_LIBRARY_DIR "${ANTLR4_RUNTIME_SHARED_LIBRARY}" DIRECTORY)
  add_library(antlr4_runtime::shared IMPORTED SHARED)
  set_target_properties(antlr4_runtime::shared
                        PROPERTIES
                          IMPORTED_LOCATION ${ANTLR4_RUNTIME_SHARED_LIBRARY}
                          IMPORTED_IMPLIB ${ANTLR4_RUNTIME_SHARED_LIBRARY}
                          INTERFACE_LINK_DIRECTORIES ${ANTLR4_RUNTIME_SHARED_LIBRARY_DIR}
                          )
  set(Antlr4Runtime_FOUND 1 CACHE INTERNAL "Antlr4Runtime found" FORCE)
else()
  message(FATAL_ERROR "Didn't find ANTLR4_RUNTIME (shared)")
endif()

unset(ANTLR4_RUNTIME_CPP_BUILD_DIR)
unset(ANTLR4_RUNTIME_CPP_BUILD_DIR_HDR)
unset(ANTLR4_RUNTIME_SHARED_LIBRARY)